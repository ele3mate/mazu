﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dropper : MonoBehaviour
{
    [SerializeField] private int dropTime = 0;

    private MeshRenderer meshRenderer;
    private new Rigidbody rigidbody;

    private void Start() 
    {
        meshRenderer = GetComponent<MeshRenderer>();
        rigidbody = GetComponent<Rigidbody>();


        meshRenderer.enabled = false;
        rigidbody.useGravity = false;

    }
    void Update()
    {
        if (Time.time > dropTime)
        {
            meshRenderer.enabled = true;
            rigidbody.useGravity = true;
        }

        
    }
}
