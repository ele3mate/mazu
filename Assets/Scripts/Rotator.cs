﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    [SerializeField] float rotatorSpeedX = 0f;
    [SerializeField] float rotatorSpeedY = 0f;
    [SerializeField] float rotatorSpeedZ = 0f;
    
    private void Update() 
    {
        transform.Rotate(rotatorSpeedX, rotatorSpeedY, rotatorSpeedZ);
    }
}
