﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorer : MonoBehaviour
{
    private int hits = 0;

    public Text scoreText;

    
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag != "Hit")
        {
            hits++;
        }



    }
    private void Update() 
    {
        scoreText.text = "Bump counter: " + hits.ToString();
    }
}

