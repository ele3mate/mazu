﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public Text timerText;
    private float startTime;
    private bool finnished = false;


    void Start()
    {
        startTime = Time.time;
    }

    void Update()
    {
        if (finnished)
        {
            return;
        }
        else
        {
            float t = Time.time - startTime;

            string seconds = (t % 60).ToString("f2");

            timerText.text = seconds;
        }    

    }

    public void Finnish()
    {
        finnished = true;
        timerText.color = Color.yellow;

    }
}
